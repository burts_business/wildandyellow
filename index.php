<?php get_header(); ?>

	<section id="about" class="cd-section shadow">
        <div class="title section">
	        <img class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>About Us</h2>
	        <i>We love creating moments for you to remember</i>
	        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>
	        <a href="#">Contact Us</a>
	    </div>
	</section>
    
    <section id="group3" class="cd-section" data-parallax="scroll" data-speed="0.2" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-second.jpg">
    </section>
    
    <section id="services" class="cd-section shadow">
        <div class="title section">
	        <img class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>What We Do</h2>
	        <i>We love creating moments for you to remember.</i>
	        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>
	        <p>It can be nice to hear from someone who has been there and done that before the day. Let us shout you a coffee and help map out your event.</p>
	        <a href="#consulting">Consulting Services</a>
	    </div>
    </section>
    
    <section id="group5" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-third.jpg">
    </section>
    
    <section id="event" class="cd-section">
		<div class="title section">
		    <img class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
		    <h2>Your Event</h2>
		    <i>Making sure everything is stunning and timed to perfection.</i>
		    <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>
		    <a href="#">Let's Talk</a>
		</div>
    </section>
    
    <section id="group7" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-fourth.jpg">   
    </section>
    
    <section id="consulting" class="cd-section">
		<div class="title section">
	        <img class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>Consulting</h2>
	        <i>We would love to shout you a coffee.</i>
	        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>
	        <a href="#">Reserve a time</a>
	    </div>
    </section>
    
    <section id="group9" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-seventh.png">
    </section>
    
    <section id="inspiration" class="cd-section">
        <div class="title section">
	        <img class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>Inspiration</h2>
	        <i>We love creating moments for you to remember</i>
	        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus.</p>
	        <a href="#">Our Favourite Things</a>
	    </div>
    </section>
    
    <section id="group11" class="cd-section shadow" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/image-lead.jpg">
    </section>
    
    <section id="contact" class="cd-section">
        <div class="title section">
	        <img class="small-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt-"logo-small" />
	        <h2>We would love to hear from you</h2>
	        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum.</p>
	        <h3>Phone:</h3>
			<a href="tel:+64 21 983 376">+64 21 983 376</a>
	        <h3>Email:</h3>
	        <a href="mailto:hello@wildandyellow.com">hello@wildandyellow.com</a>
	    </div>
    </section>
    
<?php get_footer(); ?>