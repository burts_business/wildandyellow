<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Wild &amp; Yellow</title>
    <!-- Thanks to Pure CSS parallax scroll demo #3 by Keith Clark for the Perspective Parallax --> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" />


	
   <!--Novecento-Wide-->
   	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/stylesheet.css" type="text/css" charset="utf-8" />
   	
   	<link rel="icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />
   	<link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />

 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/parallax.js"></script>
 	<script>
	 	$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
		
		
	$( document ).ready(function() {
	    $('#home-slider').css({ top: '40px'});
	});

	var fixed = false;
		$(document).scroll(function() {
		    if( $(window).scrollTop() >= 250 ) {
		        if( !fixed ) {
		           fixed = true;
		            $('.header').css({	top: '100px' });
		            
		            
		
		        }
		    } else {
		        if( fixed ) {
		            fixed = false; 
		            $('.header').css({	top: '0px' });
		            $('.mob-nav').css({	display: 'none' });
		        }
		    }
		});
		
	$(document).ready(
    function(){
        $(".mob-logo").click(function () {
            $(".mob-nav").toggle();
        });
    });
 
	$(document).ready(
    function(){
        $(".mob-nav").click(function () {
            $(".mob-nav").hide();
        });
    });
   
	$(document).ready(
    function(){
        $(".close").click(function () {
            $(".mob-nav").hide();
        });
    });

	</script>
		
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	 <div class="inner-wrap">  

		<header id="header" class="header scroll shadow">				
			<nav>
				<ul>
					<li class="left-item"><a href="#about">About Us</a></li>
					<li class="left-item"><a href="#services">What We Do</a></li>
					<li class="left-item"><a href="#event">Your Event</a></li>
					<li class="no-padd"><a href="#home"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-small.svg" alt="Wild & Yellow" title="Wild & Yellow" /></a></li>
					<li class="right-item"><a href="#contact">Contact</a></li>
					<li class="right-item"><a href="#inspiration">Inspiration</a></li>
					<li class="right-item"><a href="#consulting">Consulting</a></li>
				</ul>
				
				<li class="mobile right mob-logo"><img class="menu" src="<?php bloginfo('stylesheet_directory'); ?>/images/menu.svg" alt="menu" /> </li>
				
				<ul class="mob-nav">
					<li><a class="close" href="#about">About Us</a></li>
					<li><a class="close" href="#services">What We Do</a></li>
					<li><a class="close" href="#event">Your Event</a></li>
					<li><a class="close" href="#contact">Contact</a></li>
					<li><a class="close" href="#inspiration">Inspiration</a></li>
					<li><a class="close" href="#consulting">Consulting</a></li>

				</ul>
			</nav> 		
		</header>
		
		
					
		<div id="main-content" class="clear" role="document">
			
		
			<section id="home" class="cd-section shadow">
				
		        
			     <header class="header-home">	
		        	<nav class="left-menu">
						<ul>
							<li><a href="#about">About Us</a></li>
							<li><a href="#services">What We Do</a></li>
							<li><a href="#event">Your Event</a></li>
						</ul>
		        	</nav>
		        	<nav class="right-menu">
						<ul>
							<li><a href="#consulting">Consulting</a></li>
							<li><a href="#inspiration">Inspiration</a></li>
							<li><a href="#contact">Contact</a></li>
						</ul>
					</nav> 
			     </header>
			     <div class="title">
			        <a><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Wild & Yellow" title="Wild & Yellow" /></a>
			        <h2>Event Hosting &amp; Management</h2>
			     </div>
				
			     
			</section>
	      